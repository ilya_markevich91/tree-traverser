'use strict';

function getVisitorMethod(template, prefix, nodeType) {
    return template
        .replace('${prefix}', prefix)
        .replace('${type}', nodeType);
}

function getNodeType(node, typeField) {
    return node && node[typeField];
}

function isValidType(node, typeField) {
    return typeof getNodeType(node, typeField) === 'string';
}

function isEmptyVisitor(visitor) {
    return Object.keys(Object(visitor)).every((key) => {
        return typeof visitor[key] !== 'function';
    });
}

function getChildNodes(tree, typeField) {
    return Object.keys(tree).reduce((nodes, key) => {
        const node = tree[key];

        if (Array.isArray(node)) {
            node.forEach((arrayNode, index) => {
                if (isValidType(arrayNode, typeField)) {
                    nodes.push({ key, index, value: arrayNode });
                }
            });
        } else if (isValidType(node, typeField)) {
            nodes.push({ key, value: node });
        }

        return nodes;
    }, []);
}

function callVisitorMethod({ node, parent, controller, visitor }, { methodTemplate, typeField }, prefix, globalMethod) {
    const methodName = getVisitorMethod(methodTemplate, prefix, getNodeType(node, typeField));
    const visitorMethod = visitor[methodName] || globalMethod;

    if (typeof visitorMethod === 'function') {
        visitorMethod.call(visitor, node, parent, controller);
    }
}

function callVisitorReplaceMethod({ node, parent, controller, visitor }, { methodTemplate, typeField }, prefix, globalMethod) {
    const method = getVisitorMethod(methodTemplate, prefix, getNodeType(node, typeField));
    const visitorMethod = visitor[method] || globalMethod;

    return typeof visitorMethod === 'function' ? visitorMethod.call(visitor, node, parent, controller) : node;
}

module.exports = {
    getVisitorMethod,
    getNodeType,
    isEmptyVisitor,
    getChildNodes,
    callVisitorMethod,
    callVisitorReplaceMethod
};