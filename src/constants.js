'use strict';

module.exports = {
    states: {
        BREAK: 'break',
        CONTINUE: 'continue',
        SKIP: 'skip',
        REMOVE: 'remove'
    },
    visitorMethods: {
        ENTER: 'enter',
        LEAVE: 'leave'
    }
};
