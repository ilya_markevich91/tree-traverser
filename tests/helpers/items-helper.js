'use strict';

function createItems(itemsCount) {
    const items = [];

    for (let i = 0; i < itemsCount; i++) {
        items.push({ type: 'Type', value: 'val1' });
    }

    return items;
}

function createRecursionItem(recursionLength) {
    const item = { type: 'Type' };
    let current = item;

    for (let i = 0; i < recursionLength; i++) {
        current.value = { type: 'Type' };
        current = current.value;
    }

    return item;
}


module.exports = {
    createItems,
    createRecursionItem
};