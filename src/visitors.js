'use strict';

const { visitorMethods } = require('./constants');
const { getVisitorMethod } = require('./util');

function lowerFirst(text) {
    return text.charAt(0).toLowerCase() + text.slice(1);
}

function getNodesByTypeVisitor(type, methodTemplate) {
    const method = getVisitorMethod(methodTemplate, visitorMethods.ENTER, type);
    const resultKey = lowerFirst(type);

    return {
        [resultKey]: [],
        [method](node) {
            this[resultKey].push(node);
        }
    };
}

function getHasNodeTypeVisitor(type, methodTemplate) {
    const method = getVisitorMethod(methodTemplate, visitorMethods.ENTER, type);
    const resultKey = `has${type}`;

    return {
        [resultKey]: false,
        [method](node, parent, controller) {
            this[resultKey] = true;
            controller.break();
        }
    };
}

function getNodesByTypesVisitor(types, methodTemplate) {
    const visitor = {};

    for (let i = 0; i < types.length; i++) {
        const resultKey = lowerFirst(types[i]);
        const method = getVisitorMethod(methodTemplate, visitorMethods.ENTER, types[i]);

        visitor[resultKey] = [];
        visitor[method] = function (node) {
            this[resultKey].push(node);
        };
    }

    return visitor;
}

module.exports = {
    getNodesByTypeVisitor,
    getHasNodeTypeVisitor,
    getNodesByTypesVisitor
};