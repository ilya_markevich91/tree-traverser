'use strict';

require('should');

const { createItems, createRecursionItem } = require('../helpers/items-helper');
const { traverse, visit, replace } = require('../../src/tree-traverser');

describe('Tree traverser', () => {
    describe('#traverse', () => {
        it('should do nothing if visitor is empty', () => {
            const tree = { type: 'Type1', value: 'val1' };
            const visitor = { items: [] };

            traverse(tree, visitor).should.be.deepEqual(visitor);
        });

        it('single node', () => {
            const visitor = {
                values: [],
                enterType1 (node) {
                    this.values.push(node.value);
                }
            };
            const tree = { type: 'Type1', value: 'val1' };

            traverse(tree, visitor).values.should.be.deepEqual(['val1']);
        });

        it('multiple nodes', () => {
            const visitor = {
                values: [],
                enterType1 (node) {
                    this.values.push(node.value);
                }
            };
            const tree = {
                type: 'Type1',
                value: 'val1',
                item: { type: 'Type1', value: 'val2' },
                item2: { type: 'Type1', value: 'val3' }
            };

            traverse(tree, visitor).values.should.be.deepEqual(['val1', 'val2', 'val3']);
        });

        it('multiple nodes and skip', () => {
            const visitor = {
                values: [],
                enterType1 (node, parent, controller) {
                    this.values.push(node.value);

                    if (node.value === 'val2') {
                        controller.skip();
                    }
                }
            };
            const tree = {
                type: 'Type1',
                value: 'val1',
                item: { type: 'Type1', value: 'val2', item: { type: 'Type1', value: 'val3' } },
                item2: { type: 'Type1', value: 'val4' }
            };

            traverse(tree, visitor).values.should.be.deepEqual(['val1', 'val2', 'val4']);
        });

        it('multiple nodes and break', () => {
            const visitor = {
                values: [],
                enterType1 (node, parent, controller) {
                    this.values.push(node.value);
                    controller.break();
                }
            };
            const tree = {
                type: 'Type1',
                value: 'val1',
                item: { type: 'Type1', value: 'val2', item: { type: 'Type1', value: 'val3' } }
            };

            traverse(tree, visitor).values.should.be.deepEqual(['val1']);
        });

        it('array nodes', () => {
            const visitor = {
                values: [],
                enterType1 (node) {
                    this.values.push(node.value);
                }
            };
            const tree = {
                type: 'Type1',
                value: 'val1',
                items: [
                    { type: 'Type1', value: 'val2', item: { type: 'Type1', value: 'val3' } }
                ]
            };

            traverse(tree, visitor).values.should.be.deepEqual(['val1', 'val2', 'val3']);
        });

        it('array nodes and skip', () => {
            const visitor = {
                values: [],
                enterType1 (node, parent, controller) {
                    this.values.push(node.value);

                    if (node.value === 'val2') {
                        controller.skip();
                    }
                }
            };
            const tree = {
                type: 'Type1',
                value: 'val1',
                items: [
                    { type: 'Type1', value: 'val2', item: { type: 'Type1', value: 'val4' } },
                    { type: 'Type1', value: 'val3' }
                ]
            };

            traverse(tree, visitor).values.should.be.deepEqual(['val1', 'val2', 'val3']);
        });

        it('array nodes and break', () => {
            const visitor = {
                values: [],
                enterType1 (node, parent, controller) {
                    this.values.push(node.value);

                    if (node.value === 'val2') {
                        controller.break();
                    }
                }
            };
            const tree = {
                type: 'Type1',
                value: 'val1',
                items: [
                    { type: 'Type1', value: 'val2', item: { type: 'Type1', value: 'val4' } },
                    { type: 'Type1', value: 'val3' }
                ]
            };

            traverse(tree, visitor).values.should.be.deepEqual(['val1', 'val2']);
        });

        it('performance test', () => {
            const itemsCount = 1000000;
            const nodesCount = 2000001;

            const visitor = {
                counter: 0,
                enter () {
                    this.counter++;
                }
            };

            const tree = {
                type: 'Type',
                items: createItems(itemsCount),
                secondaryItems: createItems(itemsCount)
            };

            traverse(tree, visitor).counter.should.be.equal(nodesCount);
        });

        it('recursion test', () => {
            const itemsCount = 10000;
            const nodesCount = 10001;
            const visitor = {
                counter: 0,
                enter () {
                    this.counter++;
                }
            };
            const tree = createRecursionItem(itemsCount);

            traverse(tree, visitor).counter.should.be.equal(nodesCount);
        });
    });

    describe('#visit', () => {
        it('should throw error for empty visitor', () => {
            const tree = { type: 'Type1', value: 'val1' };
            const visitor = {};

            (() => visit(tree, visitor)).should.throw('enterType1 method is not implemented in visitor');
        });

        it('should throw error for non function method', () => {
            const tree = { type: 'Type1', value: 'val1' };
            const visitor = {
                enterType1: 'nonFunction'
            };

            (() => visit(tree, visitor)).should.throw('enterType1 method is not implemented in visitor');
        });

        it('should visit tree', () => {
            const tree = { type: 'Type1', value: 'val1', item: { type: 'Type2', value: 'val2' } };
            const visitor = {
                nodes: [],
                enterType1(node) {
                    this.nodes.push(node.value);
                    this.nodes.push(visit(node.item, visitor));

                    return this.nodes;
                },
                enterType2(node) {
                    return node.value;
                }
            };

            visit(tree, visitor).should.be.deepEqual(['val1', 'val2']);
        });
    });

    describe('#replace', () => {
        it('should do nothing if visitor is empty', () => {
            const tree = { type: 'Type1', value: 'val1' };
            const visitor = { items: [] };

            replace(tree, visitor).should.be.deepEqual(tree);
        });

        it('should replace root node', () => {
            const visitor = {
                values: [],
                enterType1 () {
                    return { type: 'Type2' };
                }
            };
            const tree = { type: 'Type1', value: 'val1' };

            replace(tree, visitor).should.be.deepEqual({ type: 'Type2' });
        });

        it('should replace one of children', () => {
            const visitor = {
                values: [],
                enterType2 () {
                    return { type: 'TypeReplaced' };
                }
            };
            const tree = { type: 'Type1', item1: { type: 'Type2' }, item2: { type: 'Type3' } };

            replace(tree, visitor).should.be.deepEqual({ type: 'Type1', item1: { type: 'TypeReplaced' }, item2: { type: 'Type3' } });
        });

        it('should replace one of array nodes', () => {
            const visitor = {
                values: [],
                enterType3 () {
                    return { type: 'TypeReplaced' };
                }
            };
            const tree = { type: 'Type1', items: [{ type: 'Type2' }, { type: 'Type3' }] };

            replace(tree, visitor).should.be.deepEqual({ type: 'Type1', items: [{ type: 'Type2' }, { type: 'TypeReplaced' }] });
        });

        it('should replace all array nodes', () => {
            const visitor = {
                values: [],
                enterType3 () {
                    return { type: 'TypeReplaced' };
                }
            };
            const tree = { type: 'Type1', items: [{ type: 'Type3' }, { type: 'Type3' }] };

            replace(tree, visitor).should.be.deepEqual({ type: 'Type1', items: [{ type: 'TypeReplaced' }, { type: 'TypeReplaced' }] });
        });

        it('should replace first of type and break', () => {
            const visitor = {
                values: [],
                enterType3 (node, parent, controller) {
                    controller.break();
                    return { type: 'TypeReplaced' };
                }
            };
            const tree = { type: 'Type1', items: [{ type: 'Type3' }, { type: 'Type3' }] };

            replace(tree, visitor).should.be.deepEqual({ type: 'Type1', items: [{ type: 'TypeReplaced' }, { type: 'Type3' }] });
        });

        it('should replace type and skip children', () => {
            const visitor = {
                values: [],
                enterType1 (node, parent, controller) {
                    controller.skip();
                    return { type: 'TypeReplaced' };
                }
            };
            const tree = { type: 'Type1', items: [{ type: 'Type3' }, { type: 'Type3' }] };

            replace(tree, visitor).should.be.deepEqual({ type: 'TypeReplaced' });
        });

        it('should replace root node and all nodes of replaced node', () => {
            const visitor = {
                values: [],
                enterType1() {
                    return { type: 'TypeReplaced1', expr: { type: 'Type2' } };
                },
                enterType2() {
                    return { type: 'TypeReplaced2' };
                }
            };
            const tree = { type: 'Type1' };

            replace(tree, visitor).should.be.deepEqual({ type: 'TypeReplaced1', expr: { type: 'TypeReplaced2' } });
        });
    });
});