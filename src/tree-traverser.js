'use strict';

const { states, visitorMethods } = require('./constants');
const { isEmptyVisitor, getNodeType, getVisitorMethod, getChildNodes, callVisitorMethod, callVisitorReplaceMethod } = require('./util');
const visitorsFactory = require('./visitors');

const defaultOptions = {
    methodTemplate: '${prefix}${type}',
    typeField: 'type'
};

function createController(defaultState) {
    return {
        state: defaultState,
        continue() {
            this.state = states.CONTINUE;
        },
        break() {
            this.state = states.BREAK;
        },
        skip() {
            this.state = states.SKIP;
        }
    };
}

function traverse(tree, visitor, opts = defaultOptions) {
    if (isEmptyVisitor(visitor)) {
        return visitor;
    }

    const controller = createController(states.CONTINUE);
    let stack = [{ node: tree, parent: null }];

    while (stack.length > 0) {
        const { node, parent, leave } = stack.pop();

        if (leave) {
            leave();
        } else {
            if (controller.state === states.BREAK) {
                break;
            }

            callVisitorMethod({ node, parent, controller, visitor }, opts, visitorMethods.ENTER, visitor.enter);

            if (controller.state === states.CONTINUE) {
                const childNodes = getChildNodes(node, opts.typeField).map((child) => ({ node: child.value, parent: node }));
                const leaveMethod = callVisitorMethod.bind(null, { node, parent, controller, visitor }, opts, visitorMethods.LEAVE, visitor.leave);

                if (childNodes.length > 0) {
                    stack = stack.concat([{ leave: leaveMethod }], childNodes.reverse());
                } else {
                    leaveMethod();
                }
            } else if (controller.state === states.SKIP) {
                controller.continue();
            }
        }
    }

    stack.reverse().filter((info) => info.leave).forEach((info) => {
        info.leave();
    });

    return visitor;
}

function visit(tree, visitor, opts = defaultOptions) {
    const methodName = getVisitorMethod(opts.methodTemplate, visitorMethods.ENTER, getNodeType(tree, opts.typeField));

    if (isEmptyVisitor(visitor) || typeof visitor[methodName] !== 'function') {
        throw new Error(`${methodName} method is not implemented in visitor`);
    }

    return visitor[methodName](tree);
}

function getResultAfterReplaceNode(tree, newNode, key, index) {
    if (!key && !index) {
        return newNode;
    } else if (key && !Number.isInteger(index)) {
        tree[key] = newNode;
        return tree;
    } else {
        tree[key][index] = newNode;
        return tree;
    }
}

function replace(tree, visitor, opts = defaultOptions) {
    if (isEmptyVisitor(visitor)) {
        return tree;
    }

    const controller = createController(states.CONTINUE);
    let stack = [{ node: tree, parent: null }];

    let result = null;

    while (stack.length > 0) {
        const { node, parent, key, index } = stack.pop();

        if (controller.state === states.BREAK) {
            break;
        }

        const newNode = callVisitorReplaceMethod({ node, parent, controller, visitor }, opts, visitorMethods.ENTER, visitor.enter);

        result = getResultAfterReplaceNode(result, newNode, key, index);

        if (controller.state === states.CONTINUE) {
            const childNodes = getChildNodes(newNode, opts.typeField).map((child) => ({
                node: child.value,
                key: child.key,
                index: child.index,
                parent: node
            }));

            if (childNodes.length > 0) {
                stack = stack.concat(childNodes.reverse());
            }
        } else if (controller.state === states.SKIP) {
            controller.continue();
        }
    }

    return result;
}

module.exports = {
    traverse,
    visit,
    replace,
    visitorsFactory
};