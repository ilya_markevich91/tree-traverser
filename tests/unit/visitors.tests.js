'use strict';

require('should');

const sinon = require('sinon');
const { getNodesByTypesVisitor, getHasNodeTypeVisitor, getNodesByTypeVisitor } = require('../../src/visitors');

const controller = {
    break: sinon.stub()
};
const template = '${prefix}${type}Expression';

describe('Visitors', () => {
    describe('#getNodesByTypesVisitor', () => {
        it('should get nodes by types visitor', () => {
            const types = ['TypeA', 'TypeB'];
            const visitor = getNodesByTypesVisitor(types, template);

            visitor.enterTypeAExpression({ type: 'TypeA' }, null, controller);
            visitor.enterTypeBExpression({ type: 'TypeB' }, null, controller);

            visitor.typeA.should.be.deepEqual([{ type: 'TypeA' }]);
            visitor.typeB.should.be.deepEqual([{ type: 'TypeB' }]);
        });
    });

    describe('#getHasNodeTypeVisitor', () => {
        it('should get has node type visitor', () => {
            const type = 'TypeA';
            const visitor = getHasNodeTypeVisitor(type, template);

            visitor.hasTypeA.should.be.equal(false);
            visitor.enterTypeAExpression({ type: 'TypeA' }, null, controller);
            visitor.hasTypeA.should.be.equal(true);
        });
    });

    describe('#getNodesByTypeVisitor', () => {
        it('should get nodes by type visitor', () => {
            const type = 'TypeA';
            const visitor = getNodesByTypeVisitor(type, template);

            visitor.enterTypeAExpression({ type: 'TypeA', value: 1 }, null, controller);
            visitor.enterTypeAExpression({ type: 'TypeA', value: 2 }, null, controller);

            visitor.typeA.should.be.deepEqual([
                { type: 'TypeA', value: 1 },
                { type: 'TypeA', value: 2 }
            ]);
        });
    });
});