'use strict';

require('should');

const sinon = require('sinon');
const { states } = require('../../src/constants');
const { getVisitorMethod, getNodeType, isEmptyVisitor, getChildNodes, callVisitorMethod, callVisitorReplaceMethod } = require('../../src/util');

describe('Util', () => {
    describe('#getVisitorMethod', () => {
        it('should get visitor method', () => {
            const template = '${prefix}${type}Expression';
            const tests = [
                ['enter', 'Number'],
                ['leave', 'Text']
            ];

            tests.map(([prefix, type]) => getVisitorMethod(template, prefix, type)).should.be.deepEqual([
                'enterNumberExpression',
                'leaveTextExpression'
            ]);
        });
    });

    describe('#getNodeType', () => {
        it('should get node type', () => {
            const tests = [
                { node: { type: 'Test' }, typeField: 'type' },
                { node: { type: 'Test' }, typeField: 'testType' }
            ];

            tests.map(({ node, typeField }) => getNodeType(node, typeField)).should.be.deepEqual([
                'Test',
                undefined
            ]);
        });
    });

    describe('#isEmptyVisitor', () => {
        it('should return false for not empty visitor', () => {
            const visitor = {
                enter() {
                    this.value = 'test';
                }
            };

            isEmptyVisitor(visitor).should.be.equal(false);
        });

        it('should return true for visitor without keys', () => {
            const visitor = {};

            isEmptyVisitor(visitor).should.be.equal(true);
        });

        it('should return true for visitor without functions', () => {
            const visitor = {
                key: 'value'
            };

            isEmptyVisitor(visitor).should.be.equal(true);
        });
    });

    describe('#getChildNodes', () => {
        const typeField = 'type';

        it('should get child nodes for non array nodes', () => {
            const tree = {
                type: 'Type',
                notNode: 1,
                node1: { type: 'Type1' },
                node2: { type: 'Type2' }
            };

            getChildNodes(tree, typeField).should.be.deepEqual([
                { key: 'node1', value: tree.node1 },
                { key: 'node2', value: tree.node2 }
            ]);
        });

        it('should get child nodes for array nodes', () => {
            const tree = {
                type: 'Type',
                nodes: [
                    { type: 'Type1' },
                    { type: 'Type2' }
                ]
            };

            getChildNodes(tree, typeField).should.be.deepEqual([
                { key: 'nodes', index: 0, value: tree.nodes[0] },
                { key: 'nodes', index: 1, value: tree.nodes[1] }
            ]);
        });

        it('should get child nodes for array nodes with hall', () => {
            const tree = {
                type: 'Type',
                nodes: [
                    { type: 'Type1' },
                    { value: 'test' },
                    { type: 'Type2' }
                ]
            };

            getChildNodes(tree, typeField).should.be.deepEqual([
                { key: 'nodes', index: 0, value: tree.nodes[0] },
                { key: 'nodes', index: 2, value: tree.nodes[2] }
            ]);
        });
    });

    describe('#callVisitorMethod', () => {
        const opts = { methodTemplate: '${prefix}${type}Expression', typeField: 'type' };

        it('should do nothing because visitor method is not defined', () => {
            const visitor = {
                enterTestExpression: sinon.mock().never()
            };
            const node = { type: 'NotTest' };

            callVisitorMethod({ node, parent: null, controller: null, visitor }, opts, 'enter');
            visitor.enterTestExpression.verify();
        });

        it('should call visitor method', () => {
            const node = { type: 'Test' };
            const parent = { type: 'Parent Type' };
            const controller = {};
            const visitor = {
                enterTestExpression: sinon.mock().withArgs(node, parent, controller)
            };

            callVisitorMethod({ node, parent, controller, visitor }, opts, 'enter');
            visitor.enterTestExpression.verify();
        });

        it('should call global method', () => {
            const node = { type: 'Test' };
            const parent = { type: 'Parent Type' };
            const controller = {};
            const visitor = {
                enter: sinon.mock().withArgs(node, parent, controller)
            };

            callVisitorMethod({ node, parent, controller, visitor }, opts, 'enter', visitor.enter);
            visitor.enter.verify();
        });
    });

    describe('#callVisitorReplaceMethod', () => {
        const opts = { methodTemplate: '${prefix}${type}Expression', typeField: 'type' };

        it('should return node because visitor method is not defined', () => {
            const node = { type: 'Test' };
            const parent = { type: 'Parent Type' };
            const controller = {};
            const visitor = {};

            callVisitorReplaceMethod({ node, parent, controller, visitor }, opts, 'enter').should.be.deepEqual(node);
        });

        it('should call visitor method', () => {
            const node = { type: 'Test' };
            const parent = { type: 'Parent Type' };
            const controller = {
                break() {
                    this.state = states.BREAK;
                }
            };
            const replaceNode = { type: 'Replaced' };
            const visitor = {
                enterTestExpression: (node, parent, controller) => {
                    controller.break();
                    return replaceNode;
                }
            };

            callVisitorReplaceMethod({ node, parent, controller, visitor }, opts, 'enter').should.be.deepEqual(replaceNode);
            controller.state.should.be.equal(states.BREAK);
        });

        it('should call global method', () => {
            const node = { type: 'Test' };
            const parent = { type: 'Parent Type' };
            const controller = {};
            const replaceNode = { type: 'Replaced' };
            const visitor = {
                enter: () => {
                    return replaceNode;
                }
            };

            callVisitorReplaceMethod({ node, parent, controller, visitor }, opts, 'enter', visitor.enter).should.be.deepEqual(replaceNode);
        });
    });
});